package wilee;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CTTPMapper extends Mapper<Object, Text, IntTriple, Edge> {
	private IntTriple mapKey = new IntTriple();
	private Edge mapValue = new Edge();
    private	IntHash hash = new IntHash();
	private int rho, r, R;
	protected void setup(Context context) throws IOException, InterruptedException{
		Configuration config = context.getConfiguration();
		//get rho
		rho = config.getInt("rho", 3);
		r = config.getInt("round", 3);
		R = config.getInt("totalR", 3);
	}
	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
		String[] pair = value.toString().split(" ");
		int u, v;
		u = Integer.parseInt(pair[0]);
		v = Integer.parseInt(pair[1]);
		
		int[] partition = new int[2];
		partition[0] = hash.getHashCode(u, rho);
		partition[1] = hash.getHashCode(v, rho);

		int i,j;
		i = partition[0] < partition[1] ? partition[0]:partition[1];
		j = partition[0] < partition[1] ? partition[1]:partition[0];

		mapValue.set(u, v);
		// If the edge is an inner edge ( 2-partition )
		if (i == j) {
			//int k0 = (r - i) % R; 
			//int k = 0;
			//for(int l = 0; k < rho; l++){
		//		k = k0 + l * R;
		//		if(k < 0 || k >= rho) continue;
			for(int k = 0; k < rho; k++){
				if ((k+i)%R == r) {
					// case wise emit
					if (k < i) {
						mapKey.set(k, i, -1);
						context.write(mapKey, mapValue);
					} else if (k > i){
						mapKey.set(i, k, -1);
						context.write(mapKey, mapValue);
					}
				}
			}
		}
		// If the edge is an outer edge ( 3-partition )
		else{
			// p(u), p(v) must be in order to avoid duplicates
			
			if((i + j) % R == r){
				mapKey.set(i, j, -1);
				context.write(mapKey, mapValue);
			}
			//int k0 = (r - i - j) % R; 
			//int k = 0;
			//for(int l = 0; k < rho; l++){
		//		k = k0 + l * R;
		//		if(k < 0 || k >= rho) continue;
			for(int k = 0; k < rho; k++){
				if ((k+i+j) % R == r) {
					// case wise emit
					if (k < i) { // k, p(u), p(v)
						mapKey.set(k, i, j);
						context.write(mapKey, mapValue);
					} else if (j < k) { // p(u), p(v), k
						mapKey.set(i, j, k);
						context.write(mapKey, mapValue);
					} else if (k != i && k != j) { // p(u), k, p(v)
						mapKey.set(i, k, j);
						context.write(mapKey, mapValue);
					}
				}
			}
		}
	}
}
