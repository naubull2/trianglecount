package wilee;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TTPDriver {
	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if(otherArgs.length!=3){
			printHelp();
			return;
		}
		// setup fixed parameters
		FileSystem fs = FileSystem.get(conf);
		Path outputDir = new Path(otherArgs[1]);
		if(fs.exists(outputDir)) fs.delete(outputDir, true);
		int numPartition = Integer.parseInt(otherArgs[2]);

		
		// broadcast parameters
		conf.setInt("rho", numPartition);
	
		// setup mapreduce job, then run
		Job job = Job.getInstance(conf, "TTP");
		job.setJarByClass(TTPDriver.class);
		
		job.setMapperClass(TTPMapper.class);
		job.setReducerClass(TTPReducer.class);
		
		job.setMapOutputKeyClass(IntTriple.class);
		job.setMapOutputValueClass(Edge.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.setNumReduceTasks(numPartition);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		if(job.waitForCompletion(true)){
			// Post processing
			// type 1 triangles need to be divided by (rho-1): due to duplications
			int type1_cnt = 0;
			int type23_cnt = 0;
			BufferedReader reader;
			for (int outidx = 0; outidx < numPartition; outidx++) {
				reader = new BufferedReader(new InputStreamReader(fs.open(new Path(otherArgs[1]	+ "/part-r-0000" + outidx))));
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] tmp = line.split("\t");
					// [type] [count]
					if (tmp[0].equals("1")) {
						type1_cnt += Integer.parseInt(tmp[1]);
					} else {
						type23_cnt += Integer.parseInt(tmp[1]);
					}
				}
			}
			int total_cnt = type1_cnt + type23_cnt;
			System.out.println("type1 : "+type1_cnt);
			System.out.println("type2,3 : "+type23_cnt);
			System.out.println("# of triangles found : " + total_cnt);
		} else {
			System.out.println("program failure");
		}
		return;
	}
	
	public static void printHelp(){
		System.out.println("Usage : [input path] [output path] [number of partition]");
	}
}
