package wilee;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TTPMapper extends Mapper<Object, Text, IntTriple, Edge> {
	private IntTriple mapKey = new IntTriple();
	private Edge mapValue = new Edge();
    private	IntHash hash = new IntHash();
	private int rho;
	protected void setup(Context context) throws IOException, InterruptedException{
		Configuration config = context.getConfiguration();
		//get rho
		rho = config.getInt("rho", 3);
	}
	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
		String[] pair = value.toString().split(" ");
		int u, v;
		u = Integer.parseInt(pair[0]);
		v = Integer.parseInt(pair[1]);
		
		int[] partition = new int[2];
		partition[0] = hash.getHashCode(u, rho);
		partition[1] = hash.getHashCode(v, rho);
	
		mapValue.set(u, v);
		// If the edge is an inner edge ( 2-partition )
		if (partition[0] == partition[1]) {
			for(int m = 0; m < partition[0]; m++){
				// < m, partition[0]>
				mapKey.set(m, partition[0], -1);
				context.write(mapKey,  mapValue);
			}
			for(int m = partition[0]+1; m < rho; m++){
				// <partition[0], m>
				mapKey.set(partition[0], m, -1);
				context.write(mapKey,  mapValue);
			}
		}

		// If the edge is an outer edge ( 3-partition )
		else{
			if( partition[1] < partition[0]){
				int tmp = partition[0];
				partition[0] = partition[1];
				partition[1] = tmp;
			}
			// m, p(u), p(v)
			for(int m = 0; m < partition[0]; m++){
				mapKey.set(m, partition[0], partition[1]);
				context.write(mapKey,  mapValue);
			}
			// p(u), m, p(v)
			for(int m = partition[0]+1; m < partition[1]; m++){
				mapKey.set(partition[0], m, partition[1]);
				context.write(mapKey, mapValue);
			}
			// p(u), p(v), m
			for(int m = partition[1]+1; m < rho; m++){
				mapKey.set(partition[0], partition[1], m);
				context.write(mapKey, mapValue);
			}
			mapKey.set(partition[0], partition[1], -1);
			context.write(mapKey,  mapValue);
		}
	}
}
