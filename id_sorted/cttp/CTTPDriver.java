package wilee;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Task.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class CTTPDriver {
	public static enum CounterEnum{INPUT_RECORDS}
	
	public static class EdgeCounterMapper extends Mapper<Object, Text, Text, IntWritable>{
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
			context.getCounter(CounterEnum.INPUT_RECORDS).increment(1);
		}
	}
	public static void main(String[] args) throws Exception{
		
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if(otherArgs.length!=4){
			printHelp();
			return;
		}
		// setup fixed parameters
		FileSystem fs = FileSystem.get(conf);
	
		int machineMem = Integer.parseInt(otherArgs[2]);   // m
		int numReducers = Integer.parseInt(otherArgs[3]);  
		int totalRounds = 0;
		long numEdges = 0;
		
		/**
		 * run MAP_ONLY for counting the number of edges
		 */
		Path counterOutDir = new Path("counter");
		if(fs.exists(counterOutDir)) fs.delete(counterOutDir, true);
		
		Job counterJob = Job.getInstance(conf, "EdgeCounter");
		counterJob.setJarByClass(CTTPDriver.class);
		counterJob.setMapOutputKeyClass(Text.class);
		counterJob.setMapOutputValueClass(IntWritable.class);	

		FileInputFormat.addInputPath(counterJob, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(counterJob, new Path("counter"));

		counterJob.setMapperClass(EdgeCounterMapper.class);
		counterJob.setNumReduceTasks(0);
		counterJob.waitForCompletion(true);
		numEdges = counterJob.getCounters().findCounter(CounterEnum.INPUT_RECORDS).getValue();
		
		// compute parameters
		int rho = (int)Math.ceil(Math.sqrt(6.0*numEdges/machineMem));
		totalRounds = (int)Math.ceil((double)(rho*numEdges)/(numReducers * machineMem));
		
		/**
		 * r-th Round
		 */
		 int tri_count = 0;
		for (int r = 0; r < totalRounds; r++) {
			// broadcast parameters
			conf.setInt("rho", rho);
			conf.setInt("round", r);
			conf.setInt("totalR", totalRounds);

			// setup mapreduce job, then run
			Job job = Job.getInstance(conf, "CTTP_"+r+"_round");
			job.setJarByClass(CTTPDriver.class);

			job.setMapperClass(CTTPMapper.class);
			job.setReducerClass(CTTPReducer.class);

			job.setMapOutputKeyClass(IntTriple.class);
			job.setMapOutputValueClass(Edge.class);
			job.setOutputKeyClass(IntWritable.class);
			job.setOutputValueClass(Text.class);

			job.setNumReduceTasks(numReducers);

			Path outputDir = new Path(otherArgs[1] + r + "-round");
			if (fs.exists(outputDir)) fs.delete(outputDir, true);
			
			FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
			FileOutputFormat.setOutputPath(job, new Path(otherArgs[1] + r + "-round"));

			if (job.waitForCompletion(true)) {
				System.out.println(r+ "-th round finished");
				tri_count += job.getCounters().findCounter(Counter.REDUCE_OUTPUT_RECORDS).getValue();
				//tri_count += job.getCounters().findCounter(CTTPReducer.MyCounter.HEARTBEAT).getValue();
			}else{
				System.out.println(r+ "-th round failure");
			}
		}
		/////// End of round iterations ///////////
		System.out.println("total triangles : "+tri_count);
		return;
	}
	
	public static void printHelp(){
		System.out.println("Usage : [input path] [output path] [m] [# reducer]");
	}
}
