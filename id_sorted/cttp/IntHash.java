package wilee;

import java.util.Random;

public class IntHash {
	//private final int prime = 17137;
	//private final int prime = 50331653;
	private final int prime = 104729;
	Random randGen = new Random(0);
    private long a = (long)randGen.nextInt(prime-1) + 1;
    private long b = (long)randGen.nextInt(prime);
    
	public int getHashCode(int x, int range){
		return (int)(((a*x+b)%prime)%range);
	}
	public int hash(int x) {
		return (int)((a*x+b)%prime);
	}
	
	public int kHash(int x, int range, int k){
		int result = x;
		for(int i = 0; i < k; i++){
			result = hash(result);
		}
		return result % range;
	}
}
