package wilee;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class IntTriple implements WritableComparable<IntTriple>{
	// partition pair a,b,c must be in order (a < b < c) to prevent duplicates
	private int a,b,c;
	
	public IntTriple(){}
	
	public IntTriple(int x, int y, int z){
		// insertion sort numbers in increasing order
		if( x < y){
			a = x;
			b = y;
		}else{
			a = y;
			b = x;
		}

		if( z < a ){
			if( z > 0){
				c = b;
				b = a;
				a = z;
			}else{ // if z == -1, this is 2-partition
				c = z;
			}
		}else if (z > b){
			c = z;
		}else{
			c = b;
			b = z;
		}
	}
	
	public void set(int x, int y, int z){
		// insertion sort numbers in increasing order
		if( x < y){
			a = x;
			b = y;
		}else{
			a = y;
			b = x;
		}

		if( z < a ){
			if( z > 0){
				c = b;
				b = a;
				a = z;
			}else{ // if z == -1, this is 2-partition
				c = z;
			}
		}else if (z > b){
			c = z;
		}else{
			c = b;
			b = z;
		}
	}
	
	public int getA(){
		return a;
	}
	public int getB(){
		return b;
	}
	public int getC(){
		return c;
	}
	
	public String toString(){
		return String.format("%d\t%d\t%d", a, b, c);
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		a = in.readInt();
		b = in.readInt();
		c = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(a);
		out.writeInt(b);
		out.writeInt(c);
	}

	public static class Comparator extends WritableComparator{
		public Comparator(){
			super(IntTriple.class);
		}
		
		public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2){
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}
	static {
		WritableComparator.define(IntTriple.class,  new Comparator());
	}
	
	
	@Override
	public int compareTo(IntTriple o) {
		int cmp1 = Integer.compare(a, o.a);
		if(cmp1 == 0){
			int cmp2 = Integer.compare(b, o.b);
			return (cmp2 == 0)? cmp2 : (Integer.compare(c, o.c));
		}
		return cmp1;
	}
	
	@Override
	public int hashCode(){
		IntHash hash = new IntHash();
		return 3 * hash.getHashCode(a, 1011) + 11 * hash.getHashCode(b, 1011) + 17 *  hash.getHashCode(c, 1011);
	}
}
