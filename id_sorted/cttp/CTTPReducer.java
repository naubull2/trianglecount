package wilee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CTTPReducer extends Reducer<IntTriple, Edge , IntWritable, Text> {
	protected static enum MyCounter{HEARTBEAT};
	private HashMap<Integer, ArrayList<Integer>> adjList = new HashMap<Integer, ArrayList<Integer>>();
	private ArrayList<Integer> setU = new ArrayList<Integer>();
	private ArrayList<Integer> setV = new ArrayList<Integer>();
	private ArrayList<Edge> edges = new ArrayList<Edge>();
	
	private int rho;
	private IntWritable outKey = null;
	private Text outValue = new Text();
	private IntHash hash = new IntHash();
	protected void setup(Context context){
		Configuration config = context.getConfiguration();
		rho = config.getInt("rho",3);
	}
	/**
	 * Runs compact-forward algorithm which assumes node ordering as the node values.
	 * Higher the node value, higher in the ordering. Finds the size of intersection of N'(u) and N'(v).
	 * The size of the intersection is the same as number of triangles in this partition.
	 * #Exception : when the key is (i, j, -1) save as different file. We will divide the sum of this type
	 * by (rho - 1) due to duplications 
	 */
	public void reduce(IntTriple key, Iterable<Edge> values, Context context) throws IOException, InterruptedException{
		// Build adjacency list
		// This special adjacency list only contains adjacent nodes whose value is less than the core point
		adjList.clear();
		edges.clear();	
		int u,v;
		for(Edge e : values){
			u = e.getU();
			v = e.getV();
			Edge edge = new Edge(u,v);
			edges.add(edge);
			int finding_key = (u < v)? v : u;
			int adding_key = (u < v)? u : v;
			
			if (adjList.containsKey(finding_key)) {
				if(!adjList.get(finding_key).contains(adding_key))
					adjList.get(finding_key).add(adding_key);
			} else { // if none exists, create a new list to the map
				ArrayList<Integer> list = new ArrayList<Integer>();
				list.add(adding_key);
				adjList.put(finding_key, list);
			}
		}
		for(Map.Entry<Integer, ArrayList<Integer>> entry : adjList.entrySet()){
			Collections.sort(entry.getValue());
		}
		
		// COMPACT-FORWARD using absolute values of the nodes
		// for each edge->(u,v)
		// find set(u), set(v)
		int nu, nv, i, j;
		for(Edge e : edges){
			u = e.getU();
			v = e.getV();
			setU = adjList.get(u);
			setV = adjList.get(v);
			// find all intersection of (u) and (v) whose values are under both u and v
			if (setU != null && setV != null) {
				int m = Math.min(u, v);	// no comparison is worth past this value (since both lists are sorted)
				for(i = 0, j = 0; i < setU.size() && j < setV.size();){
					nu = setU.get(i);
					nv = setV.get(j);
					if(nu > m || nv > m) break;

					if(nu == nv){
						// emit with the following rule
						outValue.set(i + " " + u + " " + v);
						if (hash.getHashCode(v, rho) == hash.getHashCode(u, rho) &&
								hash.getHashCode(u, rho)== hash.getHashCode(nu, rho)) { // type 1
							////TYPE 1
							int a = key.getA();
							int b = key.getB();
							int p_u = hash.getHashCode(u, rho);
							if(((a+1) == b) && (p_u == a || (b == (rho-1) && p_u == b))){
								context.write(outKey, outValue);
						//		context.getCounter(MyCounter.HEARTBEAT).increment(1);
							}
						} else { // type 2,3 triangles
							////TYPE 2 & 3
							context.write(outKey, outValue);
						//	context.getCounter(MyCounter.HEARTBEAT).increment(1);
						}
						i++;
						j++;
					}else if(nu < nv){
						i++;
					}else{
						j++;
					}
				}
				/*
				for (int i : setU) {
					if(i < v){	
						context.getCounter(MyCounter.HEARTBEAT).increment(1);
						//if (u > v && i > v) 
						//	continue;
						for (int j : setV) {
							if (i == j) {
								outValue.set(i + " " + u + " " + v);
								// For type1 triangles, setup a rule (k, k+1, -1) partition can emit
								if (hash.getHashCode(v, rho) == hash.getHashCode(u, rho) &&
										hash.getHashCode(u, rho)== hash.getHashCode(j, rho)) { // type 1
									//////////////////////////////////////
									////TYPE 1
									int a = key.getA();
									int b = key.getB();
									if(hash.getHashCode(u, rho) == a && (a + 1) == b){
										context.write(outKey, outValue);
									}else if(b == (rho-1) && hash.getHashCode(u, rho) == b && a == (b-1)){
										context.write(outKey, outValue);
									}
								} else { // type 2,3 triangles
									//////////////////////////////////////
									////TYPE 2 & 3
									context.write(outKey, outValue);
								}
							}
						}
					}
				}*/
			}
		}// end of edge iteration
	}
};
