package wilee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class TTPReducer extends Reducer<IntTriple, Edge , IntWritable, IntWritable> {
	protected static enum MyCounter{HEARTBEAT};
	private int count1, count23;
	private ArrayList<Integer> setU = new ArrayList<Integer>();
	private ArrayList<Integer> setV = new ArrayList<Integer>();
	private ArrayList<Edge> edges = new ArrayList<Edge>();
	private HashMap<Integer, Integer> degrees = new HashMap<Integer, Integer>();
	class DegreeComparator implements Comparator<Integer>{
		public HashMap<Integer, ArrayList<Integer>> adjList;
		public HashMap<Integer, Integer> degreeInfo;
		DegreeComparator(){
			adjList = new HashMap<Integer, ArrayList<Integer>>();
		}
		public void setDegreeInfo(HashMap<Integer, Integer> degrees){
			degreeInfo = degrees;
		}
		@Override
		public int compare(Integer o1, Integer o2) {
			if(degrees.get(o1.intValue()) == degrees.get(o2.intValue())){
				if(o1.intValue() > o2.intValue())
					return 1;
				else if(o1.intValue()< o2.intValue())
					return -1;
				return 0;
			}else{
				return degrees.get(o1.intValue())>degrees.get(o2.intValue())? 1:-1;
			}
		}
	}
	private DegreeComparator dComparator;
	private int rho;
	private IntWritable outKey = new IntWritable();
	private IntWritable outValue = new IntWritable();
	private IntHash hash = new IntHash();
	protected void setup(Context context){
		Configuration config = context.getConfiguration();
		rho = config.getInt("rho",3);
	}
	/**
	 * Runs compact-forward algorithm which assumes node ordering as the node values.
	 * Higher the node value, higher in the ordering. Finds the size of intersection of N'(u) and N'(v).
	 * The size of the intersection is the same as number of triangles in this partition.
	 * #Exception : when the key is (i, j, -1) save as different file. We will divide the sum of this type
	 * by (rho - 1) due to duplications 
	 */
	public void reduce(IntTriple key, Iterable<Edge> values, Context context) throws IOException, InterruptedException{
		// Build adjacency list
		// This special adjacency list only contains adjacent nodes whose value is less than the core point
		dComparator = new DegreeComparator();
		count1 = 0;
		count23 = 0;
		dComparator.adjList.clear();
		edges.clear();	
		int u,v;
		for(Edge e : values){
			u = e.getU();
			v = e.getV();
			Edge edge = new Edge(u,v);
			edges.add(edge);
			
			// add u-v
			if(dComparator.adjList.containsKey(u)){
				if(!dComparator.adjList.get(u).contains(v))
					dComparator.adjList.get(u).add(v);
			}else{
				ArrayList<Integer> list = new ArrayList<Integer>();
				list.add(v);
				dComparator.adjList.put(u, list);
			}
			// add v-u
			if(dComparator.adjList.containsKey(v)){
				if(!dComparator.adjList.get(v).contains(u))
					dComparator.adjList.get(v).add(u);
			}else{
				ArrayList<Integer> list = new ArrayList<Integer>();
				list.add(u);
				dComparator.adjList.put(v, list);
			}
			
		}
		// save degree information
		for (Edge e : edges) {
			u = e.getU();
			v = e.getV();
			degrees.put(u, dComparator.adjList.get(u).size());
			degrees.put(v, dComparator.adjList.get(v).size());
		}
		dComparator.setDegreeInfo(degrees);
		/*** KEEP only the nodes with degree lower than itself ***/
		// sort degree wise
		for (Map.Entry<Integer, ArrayList<Integer>> entry : dComparator.adjList.entrySet()) {
			ArrayList<Integer> tempList = new ArrayList<Integer>();
			for (Integer item : entry.getValue()) {
				if (entry.getKey() > item) {
					tempList.add(item);
				}
			}
			entry.setValue(tempList);
			Collections.sort(entry.getValue(), dComparator);
		}

		// COMPACT-FORWARD using absolute values of the nodes
		// for each edge->(u,v)
		// find set(u), set(v)
		int nu, nv, i, j;
		for(Edge e : edges){
			u = e.getU();
			v = e.getV();
			setU = dComparator.adjList.get(u);
			setV = dComparator.adjList.get(v);
			// find all intersection of (u) and (v) whose values are under both u and v
			if (setU != null && setV != null) {
			//	int m = Math.min(degrees.get(u), degrees.get(v));
				for(i= 0, j = 0; i < setU.size() && j < setV.size();){
					nu = setU.get(i);
					nv = setV.get(j);
				
			//		if(degrees.get(nu) > m || degrees.get(nv) > m) break;
					if (nu == nv) {
						// emit with the following rule
						//if (nu < u && u < v) {
							if (hash.getHashCode(v, rho) == hash.getHashCode(u,rho)
									&& hash.getHashCode(u, rho) == hash.getHashCode(nu, rho)) { // type 1
								int a = key.getA();
								int b = key.getB();
								int p_u = hash.getHashCode(u, rho);
								if (((a + 1) == b)
										&& (p_u == a || (b == (rho - 1) && p_u == b))) {
									count1++;
								}
							} else {
								count23++;
							}
						//}
						i++;
						j++;
					} else if (degrees.get(nu) == degrees.get(nv)) {
						if (nu < nv)
							i++;
						else
							j++;
					} else if (degrees.get(nu) < degrees.get(nv)) {
						i++;
					} else {
						j++;
					}
				}
			}
		}// end of edge iteration

		outKey.set(1); // Type1 triangles
		outValue.set(count1);
		context.write(outKey, outValue);

		outKey.set(0); // Type2 and Type3 triangles
		outValue.set(count23);
		context.write(outKey, outValue);
	}
};
