package wilee;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class Edge implements WritableComparable<Edge>{
	// edge nodes u, v must be in u < v order to prevent duplicate being treated separately
	private int u,v;
	
	public Edge(){}
	
	public Edge(int x, int y){
		if (x < y) {
			u = x;
			v = y;
		}else{
			u = y;
			v = x;
		}
	}
	
	public void set(int x, int y){
		if(x < y){
			u = x;
			v = y;
		}else{
			u = y;
			v = x;
		}
	}
	
	public int getU(){
		return u;
	}
	public int getV(){
		return v;
	}
	
	public String toString(){
		return String.format("%d\t%d", u, v);
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		u = in.readInt();
		v = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(u);
		out.writeInt(v);
	}

	public static class Comparator extends WritableComparator{
		public Comparator(){
			super(Edge.class);
		}
		
		public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2){
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}
	static {
		WritableComparator.define(Edge.class,  new Comparator());
	}
	
	
	@Override
	public int compareTo(Edge o) {
		int cmp = Integer.compare(u, o.u);
		if(cmp == 0){
			return Integer.compare(v,  o.v);
		}
		return cmp;
	}
	
	@Override
	public int hashCode(){
		IntHash hash = new IntHash();
		return hash.getHashCode(u, 1011) * 3 + hash.getHashCode(v, 1011)*17; 
	}
}
